import requests
import vlc
import stream
import cv2
import time
import base64


def stopS(device, pl):
    if device == "ip_camera":
        pl.stop()
    elif device == "webcam":
        pl.release()


def dataValid(args):

    if args['messages']['device'] == "ip_camera":
        print("Validando informacion")
        address = "rtsp://" + args['messages']['ip'] + ":" + args['messages']['port'] + "/StreamingSetting?version=1.0&action=login&userName=" + \
            args['messages']['user'] + "&password=" + args['messages']['password'] + \
            "&getRTSPStream&ChannelID=1&ChannelName=Channel1&EncoderPort=0"
        try:
            ip_status = 1
            print(requests.get("http://" +
                               args['messages']['ip'] + "/login.cs", timeout=0.1))
            pl = vlc.MediaPlayer(address)
            vlc.libvlc_video_set_callbacks(
                pl, stream._lockcb, None, stream._display, None)
            pl.video_set_format("RV32", stream.VIDEOWIDTH,
                                stream.VIDEOHEIGHT, stream.VIDEOWIDTH * 4)
            pl.play()
            time.sleep(3)
            return ip_status, pl

        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
            print("mala ip")
            ip_status = 0
            return ip_status, pl

    elif args['messages']['device'] == "webcam":  # hay que validar webcam
        pl = cv2.VideoCapture(0)
        time.sleep(3)
        ip_status = 1
        if ip_status == 1:
            # message = "green"
            # socketIO.emit('state', message)
            print("informacion validada")
        else:
            print("revisar ip")
            # message = "red"
            # socketIO.emit('state', message)
        return ip_status, pl

    elif args['messages']['device'] == "phone":  # hay que validar webcam
        # pl = cv2.VideoCapture(0)
        # aqui se abre la conexion del video web
        time.sleep(3)
        ip_status = 1
        if ip_status == 1:
            # message = "green"
            # socketIO.emit('state', message)
            print("informacion validada")
        else:
            print("revisar ip")
            # message = "red"
            # socketIO.emit('state', message)
        pl = None
        return ip_status, pl


def takePic_b64toS(args, pl):

    format = ".png"

    if args['messages']['device'] == "ip_camera":
        print("tomando foto")
        try:
            buffer = cv2.imencode(format, stream.opencvImage2)[1]
            code64 = base64.b64encode(buffer).decode('UTF-8')
            return code64

        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
            print("no se pudo enviar la foto")
            code64 = "none"
            return code64

# -------------------------------------

    elif args['messages']['device'] == "webcam":
        print("tomando foto")
        try:

            ret, image = pl.read()
            image = cv2.resize(image, (640, 360))
            buffer = cv2.imencode(format, image)[1]
            code64 = base64.b64encode(buffer).decode('UTF-8')

            return code64

        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
            print("mala ip")
            # ip_status = 0
            code64 = "none"
            return code64

    elif args['messages']['device'] == "phone":
        print("tomando foto")
        try:

            # ret, image = pl.read()
            # image = cv2.resize(image, (640, 360))
            buffer = cv2.imencode(format, image)[1]
            code64 = base64.b64encode(buffer).decode('UTF-8')

            return code64

        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
            print("mala ip")
            # ip_status = 0
            code64 = "none"
            return code64
