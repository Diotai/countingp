import cv2
import time
ip = 'http://172.20.10.14:8080/video'
cap = cv2.VideoCapture(ip)

while(cap.isOpened()):
    t1 = time.time()
    ret, frame = cap.read()
    t2 = time.time()
    cv2.imshow('frame', frame)

    print(str(t2-t1))
    height, width, channels = frame.shape
    print (height, width, channels)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
