import vlc
import libs.stream as stream
import cv2


class openVideo:
    def __init__(self, key, ip, port, user, password):
        self.key = key
        self.ip = ip
        self.port = port
        self.user = user
        self.password = password
        # print(self.key)
        if self.key == "ip_camera":
            self.address = "rtsp://" + self.ip + ":" + self.port + "/StreamingSetting?version=1.0&action=login&userName=" + \
                self.user + "&password=" + self.password + \
                "&getRTSPStream&ChannelID=1&ChannelName=Channel1&EncoderPort=0"
        # dir2 = "rtsp://192.168.1.118:554/StreamingSetting?version=1.0&action=login&userName=admin&password=Diotai123&getRTSPStream&ChannelID=1&ChannelName=Channel1&EncoderPort=0"
            self.pl = vlc.MediaPlayer(self.address)
            vlc.libvlc_video_set_callbacks(
                self.pl, stream._lockcb, None, stream._display, None)
            self.pl.video_set_format("RV32", stream.VIDEOWIDTH,
                                     stream.VIDEOHEIGHT, stream.VIDEOWIDTH * 4)
            self.pl.play()

        elif self.key == "webcam":
            self.cap = cv2.VideoCapture(0)

    def openV(self):
        if self.key == "ip_camera":
            image = stream.opencvImage2
            # print(image)
            return image, self.pl
        if self.key == "webcam":
            ret, image = self.cap.read()
            image = cv2.resize(image, (640, 360))
            return image, self.cap
        if self.key == "phone":
            print("abrir camara web")
            return image, None

    def stopS(device, pl):
        if device == "ip_camera":
            pl.stop()
        elif device == "webcam":
            pl.release()
