FROM manuelfgr/tensorflow-dlib:v1
MAINTAINER Manuel Felipe Garcia Rincon "manuel.garcia@globai.co"
ADD countingP /home/countingP
RUN pip install -r /home/countingP/requirements.txt
WORKDIR /home/countingP/
CMD ["python", "main.py"]
